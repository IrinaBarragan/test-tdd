const expect = require('chai').expect;
const {calc} = require('../src/calc');


describe("calc", () => {
    it("Should return '5' if num1 + num 2", () => {
        expect(calc(0.3, 0.6, '+')).to.equal(0.9);
    });
    it("Should return '1' if num1 - num 2", () => {
        expect(calc(3, -2, '-')).to.equal(5);
    });
    it("Should return '6' if num1 * num 2", () => {
        expect(calc(3, 2, '*')).to.equal(6);[2]
    });
    it("Should return '1.5' num1 / num 2", () => {
        expect(calc(3, 1.5, '/')).to.equal(2);
    });
    it("handles empty enre", () => {
        expect(calc()).to.contain('remake');
    });
    it("not number", () => {
        expect(calc([1, 'jfdnhsj', 6], 'string', '+')).to.contain('its not good');
    });
})